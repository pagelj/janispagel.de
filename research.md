---
layout: page
title: Research Interests
permalink: /research/
---

My research interests fluctuate between Computational Linguistics (CL) / Natural Language Processing (NLP) and Digital Humanities (DH).

They include, but are not limited to

- Anaphora resolution
  - Coreference resolution
  - Bridging resolution
- Computational literary studies
  - Detecting (proto-)types of literary characters
  - Modeling relationships of literary characters
- Domain adaptation for DH data
  - Dealing with low resource scenarios
  - Discovering and exploiting text structure
- Modeling of diachronic processes in compounds
  - Compound creation
  - Compositionality
- The combination of all these things

For more concrete examples of my research, please refer to the list of my [publications](/publications/all.html) and to the websites of the [projects](/projects) I am or was involved in.
