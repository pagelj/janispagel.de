# janispagel.de

Source code for [janispagel.de](https://janispagel.de).

The site is built using [Jekyll](https://jekyllrb.com/) with a modified version of the [Minima](https://github.com/jekyll/minima) theme. Some if the icons are coming from [Academicons](https://jpswalsh.github.io/academicons/).
