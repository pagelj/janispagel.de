---
layout: home
---

<div class="profile-pic"><img src="/assets/janis.jpg" alt="Janis Pagel" width="150" height="150"/></div>

I am a computational linguist and currently postdoc at the [Department of Digital Humanities](https://dh.phil-fak.uni-koeln.de/en) at the [University of Cologne](https://portal.uni-koeln.de/en/uoc-home), Germany.

<div style="clear:left">&nbsp;</div>

## Email

{% if site.email %}
  <p><a class="u-email" href="mailto:{{ site.email }}">{{ site.email }}</a></p>
{% endif %}

## PGP

My [GnuPG](https://gnupg.org/)/[OpenPGP](https://www.openpgp.org/) specifications:

| Key-ID | `1742C5D7` |
| Fingerprint | `2AF4 BFAC 3E33 CF1D C83C CE65 B44F 20A6 1742 C5D7` |
| Public key file | [`1742C5D7_pub.asc`]({{site.baseurl}}/assets/1742C5D7_pub.asc) |

## Profiles on External Sites

<p>
<div class="social-links">
  {%- include social.html -%}
</div>
</p>

## Memberships and Associations

| Association                             | Status         | Since |
| -----                                   | -----          | ----- |
| [SIGHUM](https://sighum.wordpress.com/) | Member         | 2022  |
| [CRETA e.V.](https://cretaverein.de/)   | Regular member | 2020  |
| [DHd](https://dig-hum.de/ueber-dhd)     | Regular member | 2019  |
| [ACL](https://www.aclweb.org)           | Regular Member | 2018  |
