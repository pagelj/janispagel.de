---
layout: page
title: Impressum/About
permalink: /about/
---

Angaben gemäß § 5 TMG und Art. 13 DSGVO

## Kontakt/Contact

Janis Pagel  
Pfaffenwaldring 5b  
70569 Stuttgart  
Tel.: +49 711 68581389  
Email: janis.pagel@ims.uni-stuttgart.de

## Inhaltlich Verantwortlich/Responsible for the Content

Janis Pagel  
Pfaffenwaldring 5b  
70569 Stuttgart  
Tel.: +49 711 68581389  
Email: janis.pagel@ims.uni-stuttgart.de

## Datenschutz/Privacy

Ich speichere keine Informationen irgendeiner Art über die Besucher dieser Seite. Mein Webhoster speichert verschiedene Arten von nicht-anonymisierten Informationen für maximal 14 Tage, wie etwa die IP-Adresse, Nutzung von eingehenden und ausgehenden Links, Browserart und Versionsnummer sowie Datum und Uhrzeit des Zugriffs. Informationen in anonymisierter Form werden für maximal 3 Monate aufbewahrt. Anonymisiert bedeutet hier, dass die letzte Ziffer der IP-Adresse durch eine 0 ersetzt wird. Ich selbst kann nur die anonymisierten Informationen einsehen, speichere diese jedoch nicht.

I do not store information of any kind about the visitors of this webpage. My web host stores different kind of non-anonymised information for a maximum of 14 days, such as IP address, use of ingoing and outgoing links, browser type and version as well as date and time of the access. Information in anonymised form is stored for a maximum of 3 months. Anonymised means that the last digit of the visitor's IP address is replaced with 0. I am only able to access the anonymised information, but do not save it.
