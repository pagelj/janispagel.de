build: SHELL := /bin/bash --login
build: *.md
	rvm use 2.7.2 && bundle exec jekyll build

serve: SHELL := /bin/bash --login
serve: *.md
	rvm use 2.7.2 && bundle exec jekyll serve

deploy: test build deploy.sh Rakefile
	sh deploy.sh

test: Rakefile
	rake test

all: build test deploy
