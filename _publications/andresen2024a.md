---
layout: pub
type: article
title: "Knowledge Distribution in German Drama"
author:
- Melanie Andresen
- Benjamin Krautter
- Janis Pagel
- Nils Reiter
journal: Journal of Open Humanities Data
volume: 10
number: 1
spage: 1
epage: 7
month: 01
iurl: "https://openhumanitiesdata.metajnl.com/articles/10.5334/johd.167"
doi: 10.5334/johd.167
year: 2024
---

## Abstract

What do characters in theater plays know about character relations, and how does the distribution of knowledge evolve over the course of a play? We present a dataset of 30 German plays annotated with information about the distribution of knowledge about character relations (such as "A learns from B that C is the parent of D"). All plays were manually annotated by two independent annotators as part of the Q:TRACK research project, which aims to systematically model character knowledge. The dataset is available on GitHub and Zenodo and can be reused, for example, for systematic studies of knowledge in plays or for analyzing disagreements between annotators.
