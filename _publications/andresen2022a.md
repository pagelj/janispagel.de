---
layout: pub
type: inproceedings
title: "Nathan nicht ihr Vater? – Wissensvermittlungen im Drama annotieren"
author:
- Melanie Andresen
- Benjamin Krautter
- Janis Pagel
- Nils Reiter
month: 3
location: Potsdam, Germany
booktitle: Book of Abstracts of DHd
spage: 186
epage: 190
year: 2022
doi: 10.5281/zenodo.6327913
---

## Abstract

Die quantitative Dramenanalyse hat sich lange Zeit auf formale Merkmale der Textoberfläche konzentriert. Das Projekt Q:TRACK widmet sich einer stärker inhaltlich fokussierten Erschließung von Dramen, genauer Prozessen der Vermittlung von Wissen über Familienrelationen der Figuren. Das (fehlende) Wissen über Verwandtschaftsverhältnisse ist für zahlreiche deutschsprachige Dramen des 18. und 19. Jahrhunderts entscheidendes Element der Handlung, sodass sich eine systematische Untersuchung aufdrängt. In diesem Beitrag gehen wir zunächst auf die Bedeutung von Wissen und Wissensvermittlungen für die Handlung wie auch die Wirkung von Dramen ein. Anschließend beschreiben wir, wie solche Prozesse der Wissensvermittlung in Annotationen erfasst und modelliert werden können. Am Beispiel von Gotthold Ephraim Lessings hochkanonischem Drama Nathan der Weise (1779) zeigen wir, wie sich die Analyse eines Dramas auf diese Annotationen aufbauen lässt. Im Fazit blicken wir auf Perspektiven für die Automatisierung und die quantitative Analyse größerer Dramenbestände.

