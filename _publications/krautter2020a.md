---
layout: pub
type: article
title: "„[E]in Vater, dächte ich, ist doch immer ein Vater“. Figurentypen und ihre Operationalisierung"
author:
- Benjamin Krautter
- Janis Pagel
- Nils Reiter
- Marcus Willand
month: 12
iurl: http://www.zfdg.de/2020_007
doi: 10.17175/2020_007
journal: Zeitschrift für digitale Geisteswissenschaften (ZfdG)
volume: 5
number: 7
year: 2020
---

## Abstract

Dieser Artikel behandelt die Operationalisierung von Figurentypen im deutschsprachigen Drama. Ausgehend von der dramen- und theatergeschichtlichen Forschung werden Figuren bestimmt, die einem der drei Figurentypen ›Intrigant*in‹, ›tugendhafte Tochter‹ und ›zärtlicher Vater‹ entsprechen. Für die Figuren wurden eigenschaftsbasierte Datensätze erstellt, die zu ihrer automatischen Klassifikation herangezogen werden. Neben die inhaltliche Komplexität von Figuren und die theoretische Bestimmung von Figurentypen tritt die methodische Herausforderung, ausgehend von einer kleinen Menge an Annotationen zu generalisieren. Unsere Experimente zeigen, dass sich die gewählten Typen jeweils innerhalb einer Grundgesamtheit als abgrenzbare Teilmengen herausbilden.

This article deals with the operationalization of character types in German-language drama. Based on literary and theatre history, we identify characters that belong to one of the three types: ›schemer‹, ›virtuous daughter‹ and ›tender father‹. For the characters in our corpus, property-based data sets were established, which are then used in turn for their automatic classification. In addition to the complexity of characters and the theoretical determination of character types, we discuss the methodological challenge of generalizing from a small set of annotations. Our experiments show that the selected types emerge as definable subsets within a population. 
