---
layout: pub
type: inproceedings
title: "Passive Präsenz tragischer Hauptfiguren im Drama"
author:
- Marcus Willand
- Benjamin Krautter
- Janis Pagel
- Nils Reiter
month: 3
location: Paderborn, Germany
booktitle: Book of Abstracts of DHd
spage: 177
epage: 181
editor: Christof Schöch
year: 2020
doi: 10.5281/zenodo.4621812
---

## Abstract

Der Vortrag stellt einen Versuch vor, Figurenpräsenz in dramatischen Texten auch als "passive Präsenz" zu modellieren, d.h. greifbar zu machen, dass gerade auf Hauptfiguren auf sehr unterschiedliche Weise referiert wird, wenn diese gerade nicht selbst handeln. Dazu stellen wir eine Operationalisierung von „passiver Präsenz“ vor und vergleichen die aktive und passive Präsenz von Hauptfiguren in unterschiedlichen dramatischen Gernres und Epochen. Die identifizierbare gattungsspezifische Präsenzgestaltung von Hauptfiguren lässt auf grundlegend divergierende Bauprinzipien dramatischer Text schließen. Da diese Unterschiede beim linearen Lesen jedoch kaum sichtbar sind, möchte dieser Forschungsbeitrag als Argument für die Erweiterung der qualitativ-interpretierenden Dramenanalyse durch quantitative Methoden verstanden werden.
