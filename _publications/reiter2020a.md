---
layout: pub
type: incollection
title: "Reaching out: Interdisziplinäre Kommunikation und Dissemination"
author:
- Nils Reiter
- Gerhard Kremer
- Kerstin Jung
- Benjamin Krautter
- Janis Pagel
- Axel Pichler
booktitle: "Reflektierte Algorithmische Textanalyse. Interdisziplinäre(s) Arbeiten in der CRETA-Werkstatt"
editor:
- Nils Reiter
- Axel Pichler
- Jonas Kuhn
address: Berlin
publisher: De Gruyter
spage: 467
epage: 484
month: 7
year: 2020
doi: 10.1515/9783110693973-019
---

## Abstract

In der Computerlinguistik (CL) wird Annotation mit dem Ziel eingesetzt, Daten als Grundlage für maschinelle Lernansätze und Automatisierung zu sammeln. Gleichzeitig nutzen Geisteswissenschaftler Annotation in Form von Notizen beim Lesen von Texten. Wir behaupten, dass mit der Entwicklung der Digital Humanities (DH) die Annotation zu einer Methode geworden ist, die als Mittel zur Unterstützung der Interpretation und Entwicklung von Theorien eingesetzt werden kann. In diesem Beitrag zeigen wir, wie diese verschiedenen Annotationsziele in einem einheitlichen Workflow abgebildet werden können. Wir reflektieren die Komponenten dieses Workflows und geben Beispiele, wie Annotation im Rahmen von DH-Projekten einen Mehrwert schaffen kann.

In computational linguistics (CL), annotation is used with the goal of compiling data as the basis for machine learning approaches and automation. At the same time, in the Humanities scholars use annotation in the form of note-taking while reading texts. We claim that with the development of Digital Humanities (DH), annotation has become a method that can be utilized as a means to support interpretation and develop theories. In this paper, we show how these different annotation goals can be modeled in a unified workflow. We reflect on the components of this workflow and give examples for how annotation can contribute additional value in the context of DH projects.
