---
layout: pub
type: inproceedings
title: "Detecting Protagonists in German Plays around 1800 as a Classification Task"
author:
- Nils Reiter
- Benjamin Krautter
- Janis Pagel
- Marcus Willand
location: "Galway, Ireland"
month: 12
doi: 10.18419/opus-10162
booktitle: Proceedings of the European Association for Digital Humantities (EADH)
year: 2018
iurl: https://elib.uni-stuttgart.de/bitstream/11682/10179/1/article.pdf
---

## Abstract

In this paper, we aim at identifying protagonists in plays automatically. To this end, we train a classifier using various features and investigate the importance of each feature. A challenging aspect here is that the number of spoken words for a character is a very strong baseline. We can show, however, that a) the stage presence of characters and b) topics used in their speech can help to detect protagonists even above the baseline.

