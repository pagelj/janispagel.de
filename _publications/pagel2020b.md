---
layout: pub
type: incollection
title: Annotation als flexibel einsetzbare Methode
author:
- Janis Pagel
- Nils Reiter
- Ina Rösiger
- Sarah Schulz
booktitle: "Reflektierte Algorithmische Textanalyse. Interdisziplinäre(s) Arbeiten in der CRETA-Werkstatt"
editor:
- Nils Reiter
- Axel Pichler
- Jonas Kuhn
address: Berlin
publisher: De Gruyter
spage: 125
epage: 142
month: 7
year: 2020
doi: 10.1515/9783110693973-006
---

## Abstract

In diesem Kapitel diskutieren wir Aktivitäten, die wir im Rahmen von CRETA etabliert haben, um einerseits die interdisziplinäre Kommunikation zu verbessern und andererseits CRETA-Erkenntnisse nach außen zu tragen. Konkret stellen wir ein mehrfach durchgeführtes hackatorial (Workshop „Maschinelles Lernen lernen“), einen Workshop zur Operationalisierung als Kernaufgabe für die Digital Humanities, sowie das CRETA-Coaching vor. Zu allen Aktivitäten sammeln wir unsere Ergebnisse und Erfahrungen in einem Fazit.

This chapter presents various activities related to internal and external communication, including activities related to the dissemination of ideas developed in CRETA. Specifically, we present the ‘hackatorial’ (workshop “Learning machine learning”), a ‘workshop on operationalization’ as a core task for the digital humanities, and the ‘CRETA coaching’. For all activities we collect our results and experiences in a conclusion.
