---
layout: pub
type: phdthesis
title: "Enhancing character type detection using coreference information: experiments on dramatic texts"
author:
- Janis Pagel
school: University of Stuttgart
year: 2024
downloads:
- desc: "thesis"
  url: http://dx.doi.org/10.18419/opus-15000
---
