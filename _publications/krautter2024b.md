---
layout: pub
type: inproceedings
title: "Micro- and Macroanalytical Perspectives on Knowledge Transmission in German-Language Plays (1740–1910)"
author:
- Benjamin Krautter
- Melanie Andresen
- Janis Pagel
- Nils Reiter
month: 8
location: Washington, D.C., USA
booktitle: Book of Abstracts of DH
year: 2024
iurl: "https://dh24-abstracts.netlify.app/assets/krautter_benjamin_micro__and_macroanalytical_perspectives_on"
---

## Abstract

In this paper, we present two approaches to model our data about knowledge transmissions in German-language plays in light of typical research questions of computational literary studies (CLS). Firstly, we suggest utilizing our data to calculate network metrics. Secondly, we propose to model our data as knowledge graph.
