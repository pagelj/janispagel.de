---
layout: pub
type: bachelorsthesis
title: "Automatische Belebtheitsklassifikation im Deutschen"
author:
- Janis Pagel
school: Ruhr-University Bochum
status: unpublished
year: 2015
downloads:
- desc: "thesis"
  url: /assets/publications/pagel2015a/thesis.pdf
- desc: "data"
  url: http://www.sfs.uni-tuebingen.de/ascl/ressourcen/corpora/tueba-dz.html
---
