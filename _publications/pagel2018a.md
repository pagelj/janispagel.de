---
layout: pub
type: inproceedings
title: "Towards bridging resolution in German: Data analysis and rule-based experiments"
author:
- Janis Pagel
- Ina Rösiger
location: "New Orleans, LA, USA"
month: 06
iurl: http://aclweb.org/anthology/W18-0706
downloads:
- desc: "supp"
  url: https://www.ims.uni-stuttgart.de/institut/team/documents/alt-nicht-mehr-da/roesigia/bridging-resolution-german-supplementary.pdf
- desc: "data1"
  url: https://www.ims.uni-stuttgart.de/forschung/ressourcen/korpora/dirndl.en.html
- desc: "data2"
  url: http://hdl.handle.net/11022/1007-0000-0007-C632-1
- desc: "code"
  url: https://github.com/InaRoesiger/BridgingSystem/tree/master/German
booktitle: Proceedings of the Workshop on Computational Models of Reference, Anaphora, and Coreference (CRAC)
spage: 50
epage: 60
year: 2018
---

## Abstract

Bridging resolution is the task of recognising bridging anaphors and linking them to their antecedents. While there is some work on bridging resolution for English, there is only little work for German. We present two datasets which contain bridging annotations, namely DIRNDL and GRAIN, and compare the performance of a rule-based system with a simple baseline approach on these two corpora. The performance for full bridging resolution ranges between an F1 score of 13.6% for DIRNDL and 11.8% for GRAIN. An analysis using oracle lists suggests that the system could, to a certain extent, benefit from ranking and re-ranking antecedent candidates. Furthermore, we investigate the importance of single features and show that the features used in our work seem promising for future bridging resolution approaches.
