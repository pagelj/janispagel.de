---
layout: pub
type: article
title: "Titelhelden und Protagonisten - Interpretierbare Figurenklassifikation in deutschsprachigen Dramen"
author:
- Benjamin Krautter
- Janis Pagel
- Nils Reiter
- Marcus Willand
month: 11
iurl: https://www.digitalhumanitiescooperation.de/wp-content/uploads/2018/12/p07_krautter_et_al.pdf
downloads:
- desc: "transl"
  url: https://www.digitalhumanitiescooperation.de/wp-content/uploads/2019/07/p07_krautter_et_al_eng.pdf
- desc: "blog"
  url: https://quadrama.github.io/blog/2018/12/12/detect-protagonists.en
journal: LitLab Pamphlets
volume: 7
year: 2018
---

## Abstract

In den Literaturwissenschaften koexistieren verschiedene Perspektiven auf Protagonisten, Helden oder Hauptfiguren dramatischer Texte, die unterschiedliche Definitionen und Identifikationsstrategien der Figuren veranschlagen. Grundsätzlich lassen sich die meisten dieser Definitionen in ein Set computerlesbarer Figureneigenschaften übersetzen. Figuren, die diesen Merkmalen entsprechen, können dann etwa als Protagonisten des Dramas klassifiziert und von anderen Figuren (wie etwa Nebenfiguren) unterschieden werden. Eine solche Klassifikationsaufgabe ist das zentrale Anliegen des Beitrags. Ein Teilproblem stellt dabei die Erkennung von titelgebenden Figuren dar, die zwar mit der Protagonistenklassifikation verwandt ist, aber eigene Voraussetzungen mit sich bringt. Wir nähern uns beiden Aufgaben zunächst theoretisch und schlagen eine eigene Protagonistendefinition vor, die sich zum Zweck einer automatischen Klassifikation operationalisieren lässt, die aber dennoch bestehende literaturwissenschaftliche Forschung aufgreift und an deren Definitionen anschließt. Ein manueller Annotationsversuch zeigt gleichwohl, dass Definitionen dieser Art nur begrenzt intersubjektivierbar sind. Mithilfe von verschiedenen Features wie Tokenzahl von Figuren, Topic Modeling und Netzwerkmaßen trainieren wir anschließend einen Random Forest Classifier, der Figuren automatisiert in Protagonisten und Nicht-Protagonisten, bzw. Titelfiguren und Nicht-Titelfiguren aufteilt. Die Ergebnisse zeigen, dass Protagonisten und Titelfiguren aufgrund ihrer meist herausgehobenen Stellung im Drama tatsächlich sehr sicher mit einfachen Features zu erkennen sind. Eine abschließende Analyse der Klassifikation einzelner Figuren am Beispiel von Die Verschwörung des Fiesko zu Genua, Maria Stuart und Emilia Galotti schließt an die literaturwissenschaftliche Perspektive an und macht deutlich, dass Machine Learning Modelle interessante Ausgangspunkte für tiefergehende Überlegungen zu Protagonisten und Titelfiguren bereithalten.


Within literary studies, there is a coexistence of different perspectives on protagonists, heroes or main characters in dramatic texts, which provide different definitions and strategies for the identification of those characters. Essentially , most of these definitions can be translated into a set of machine-readable character traits. Characters that correspond to these traits may then be classified as protagonists of the drama in question, and be distinguished from other characters (e.g. minor, secondary, supporting characters). Designing an applicable classification is the central objective of this article. Part of the problem lies in identifying eponymous characters, which is related to classifying protagonists, but involves its own presuppositions. We start by approaching both tasks from a theoretical perspective and suggest our own definition of a protagonist, which can be operationalized for the purpose of machinable classification but still draws on existing research in literary studies and follows its definitions. An attempt at manual annotation shows however that this type of definition possesses only a limited potential for intersubjectivity. Using a variety of features such as token count of characters, topic modeling and network sizes, we then train a random forest classifier that separates characters into protagonists and non-protagonists or eponymous heroes andn on-eponymous heroes, respectively. The results show that protagonists and eponymous heroes are in fact reliably identifiable using simple features because of their usually prominent position within the play. Following the literary studies perspective, ac onclusive analysis of the classification of specific characters using the examples of Die Verschwörung des Fiesko zu Genua, Maria Stuart, and Emilia Galotti makes clear that machine learning models offer interesting starting points for more in-depth refections on protagonists and eponymous heroes.
