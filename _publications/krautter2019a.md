---
layout: pub
type: inproceedings
title: "Klassifikation von Titelfiguren in deutschsprachigen Dramen und Evaluation am Beispiel von Lessings „Emilia Galotti“"
author:
- Benjamin Krautter
- Janis Pagel
month: 3
location: Frankfurt am Main, Germany
doi: 10.5281/zenodo.4622195
spage: 160
epage: 164
booktitle: Book of Abstracts of DHd
year: 2019
iurl: https://elib.uni-stuttgart.de/bitstream/11682/10382/1/KRAUTTER_Benjamin_Klassifikation_von_Titelfiguren_in_deutsch.pdf
---

## Abstract

Der Idee einer quantitativen und zugleich multidimensionalen Einteilung dramatischer Figuren folgend versuchen wir Titelfiguren im deutschsprachigen Drama automatisch zu bestimmen. Dazu fassen wir das Problem als Klassifikationsaufgabe, die mit maschinellen Lernverfahren bearbeitet wird. Als Features nutzen wir die gesprochenen Tokens der Figuren, deren Bühnenpräsenz, Netzwerkmetriken, Topic Modeling und einige Metadaten.  
Wir können zeigen, dass unser multidimensionales Modell sinnvolle Ergebnisse für die Klassifikation titelgebender Figuren liefert: MCC 0.66. Titelfiguren werden sehr zuverlässig erkannt (Recall 1.00), das Modell neigt jedoch zur Übergeneralisierung. Wir evaluieren diese Klassifikationsergebnisse anhand von Lessings „Emilia Galotti“.
