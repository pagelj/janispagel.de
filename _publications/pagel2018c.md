---
layout: pub
type: inproceedings
title: "A Unified Text Annotation Workflow for Diverse Goals"
author:
- Janis Pagel
- Nils Reiter
- Ina Rösiger
- Sarah Schulz
location: "Sofia, Bulgaria"
month: 8
iurl: http://ceur-ws.org/Vol-2155/pagel.pdf
booktitle: Proceedings of the Workshop for Annotation in Digital Humanities (annDH)
editor:
- Sandra Kübler
- Heike Zinsmeister
spage: 31
epage: 36
year: 2018
---

## Abstract

In computational linguistics (CL), annotation is used with the goal of compiling data as the basis for machine learning approaches and automation. At the same time, in the Humanities scholars use annotation in the form of note-taking while reading texts. We claim that with the development of Digital Humanities (DH), annotation has become a method that can be utilized as a means to support interpretation and develop theories. In this paper, we show how these different annotation goals can be modeled in a unified workflow. We reflect on the components of this workflow and give examples for how annotation can contribute additional value in the context of DH projects.

