---
layout: pub
type: incollection
title: "The Schemer in German Drama. Identification and Quantitative Characterization"
author:
- Benjamin Krautter
- Janis Pagel
booktitle: "Computational Drama Analysis. Reflecting on Methods and Interpretations"
editor:
- Melanie Andresen
- Nils Reiter
address: Berlin
publisher: De Gruyter
spage: 123
epage: 148
month: 06
year: 2024
doi: 10.1515/9783111071824-007
---

## Abstract

Dramatic characters frequently fill out different role types and act ac- cording to traits conventionally attributed to their role. One of these role types is the "schemer," characterized by intervening in a play's main plot and driving forward the plot's main conflicts. In our study, we utilized secondary literature to identify 50 characters as schemers and extracted a wide range of features which are likely to distinguish "schemers" from "non-schemers." Using machine learning, we trained a model to automatically classify characters according to these two classes and performed a number of analyses in order to identify the most contributing features. Our model is able to reliably detect schemers, utilizing features that cover information about stage presence and content of character speech, but exhibits a rather low precision. We show that this can partially be attributed to the heterogeneous nature that characterizes the group of schemers.
