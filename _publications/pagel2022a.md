---
layout: pub
type: inproceedings
title: "Co-reference networks for dramatic texts: Network analysis of German dramas based on co-referential information"
author:
- Janis Pagel
location: "Tokyo, Japan (Online)"
month: 7
spage: 326
epage: 329
booktitle: "Book of Abstracts of DH2022"
doi: "10.5281/zenodo.10236154"
year: 2022
---

## Abstract

In this abstract, we construct social networks based on co-reference information for German plays and compare these networks to co-presence networks. It can be seen that both types of networks provide complementary information about the importance of characters in a play.
