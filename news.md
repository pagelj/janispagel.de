---
layout: page
title: News
permalink: /news/
---

<h3><div class="feed-subscribe"><a href="/feed.xml"><svg class="svg-icon"><use xlink:href="{{ '/assets/minima-social-icons.svg#rss' | relative_url }}"/></svg> Atom feed</a></div></h3>

{%- if site.posts.size > 0 -%}
    
		<ul class="post-list">
      {%- for post in site.posts -%}
      <li>
        <h3>
          {{ post.title | escape }}
        </h3>
        {%- assign date_format = site.minima.date_format | default: "%b %-d, %Y" -%}
        <span class="post-meta">{{ post.date | date: date_format }}</span>
        {{ post.excerpt | markdownify }}
      </li>
      {%- endfor -%}
    </ul>

{%- endif -%}
