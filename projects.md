---
layout: page
title: Projects
permalink: /projects/
keywords:
- projects
- funding
---

## Current

{% for project in site.data.projects.current %}
{% assign inst = site.data.locations[project.location] %}
{% assign fund = site.data.funding[project.funding] %}

<h3>{{ project.title }}<img class="logo" alt="Project Logo" src="{{ project.logo }}" /></h3>

- Full project name: {{project.subtitle}}
- Institute: [{{inst.name}}]({{ inst.link }}), {{inst.country}}
- Role: {{project.role }}
- Since: {{project.time}}{% if project.link %}
- Homepage: [{{project.link}}]({{ project.link }}){%endif%}
{% if project.funding %} - Funded by: [{{ fund.name }}]({{ fund.homepage }}){% endif %}

{% endfor %}

<hr/>

## Past

{% for project in site.data.projects.completed %}
{% assign inst = site.data.locations[project.location] %}
{% assign fund = site.data.funding[project.funding] %}

<h3>{{ project.title }}<img class="logo" alt="Project Logo" src="{{ project.logo }}" /></h3>

- Full project name: {{project.subtitle}}
- Institute: [{{inst.name}}]({{ inst.link }}), {{inst.country}}
- Role: {{project.role }}
- Duration: {{project.time}}{% if project.link %}
- Homepage: [{{project.link}}]({{ project.link }}){%endif%}
{% if project.funding %} - Funded by: [{{ fund.name }}]({{ fund.homepage }}){% endif %}

{% endfor %}
