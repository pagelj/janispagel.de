---
layout: post
title: Talk at the 9th Hildesheim-Göttingen-Workshop on DH and CL
categories: news
---

Benjamin Krautter and I are giving a talk at the [9th Hildesheim-Göttingen-Workshop on DH and CL](https://www.gcdh.de/en/events/categories/event-details-categories/?tx_news_pi1%5Bnews%5D=92&tx_news_pi1%5Bcontroller%5D=News&tx_news_pi1%5Baction%5D=detail&cHash=7c6360cce0cb5368f1eb19532e3316a8)
on identifying character types, taking place in Göttingen, February 27, 2020 at 9:10 a.m.
