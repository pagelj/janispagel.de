---
layout: post
title: Talk at EADH 2018, Galway
categories: news
---

I visited the [EADH 2018](https://eadh2018eadh.wordpress.com/) in Galway, Ireland and presented a [paper](/publications/reiter2018a.html) on protagonist detection.
