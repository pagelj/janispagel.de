---
layout: post
title: Blog post on DHdBlog / Travel stipend
categories: news
---

A [blog post](https://dhd-blog.org/?p=13737) written by me got published on the [DHdBlog](https://dhd-blog.org).
The post describes my experiences at the DHd 2020 conference (https://dhd2020.de/) in Paderborn and was part of a [travel stipend](https://dhd-blog.org/?p=13081) I received from the [DHd society](https://dig-hum.de/) for travelling to this conference.
